package mx.itesm.feb14;

public class Student {

    // java beans
    // class that only contains data
    private String name;
    private float grade;
    private long studentId;

    public Student(String name, float grade, long studentId){
        this.name = name;
        this.grade = grade;
        this.studentId = studentId;
    }

    public String getName(){ return name; }

    public float getGrade() { return grade; }

    public long getStudentId() { return studentId; }
}
