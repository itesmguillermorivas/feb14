package mx.itesm.feb14;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class CustomAdapter extends BaseAdapter {

    private ArrayList<Student> source;
    private Activity activity;

    public CustomAdapter(ArrayList<Student> source, Activity activity){
        this.source = source;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return source.size();
    }

    @Override
    public Object getItem(int position) {
        return source.get(position);
    }

    @Override
    public long getItemId(int position) {
        return source.get(position).getStudentId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // view - the actual specific row
        if(convertView == null){
            // inflate - from xml to java object
            convertView = activity.getLayoutInflater().inflate(R.layout.row, null);
        }

        TextView name = convertView.findViewById(R.id.name);
        TextView grade = convertView.findViewById(R.id.grade);

        Student theStudent = source.get(position);
        name.setText(theStudent.getName());
        grade.setText(theStudent.getGrade() + "");

        return convertView;
    }
}
